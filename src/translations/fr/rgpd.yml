title: Vie privée, données personnelles et RGPD
md: |- # ⚠️ identation 2 espaces ⚠️
  Ce document rassemble les informations générales concernant la façon dont Framasoft gère les questions de vie privée, notamment concernant la gestion de vos données personnelles, sur les différents sites web de la plateforme @:(txt.space). Il s’agit d’un document nous permettant d’être *en principe* en accord avec le RGPD (Règlement Général sur la Protection des Données).

  Dans un souci de transparence et de facilitation de compréhension, nous vous proposons une version courte, lisible et nous l’espérons compréhensible de cette politique de confidentialité, suivi d’une version plus longue (et réglementairement correcte).

  Notez aussi qu’il existe un autre document, qui peut sembler redondant, nommé « [Conditions spécifiques d’hébergement des données à caractère personnel](https://www.frama.space/abc/fr/csu/) ». Alors que le document que vous êtes en train de lire est un document **informatif concernant tous les publics**, ce second document est, lui, un contrat, qui lie **juridiquement et contractuellement Framasoft et les utilisateurs et utilisatrices de la plateforme @:txt.space**.

  ## Version courte (non règlementaire)

  Framasoft s’engage à respecter vos données, personnelles comme non personnelles.

  Nous nous efforçons de minimiser le nombre de données personnelles recueillies, tout simplement parce que, au delà même de la loi ou du Règlement Général sur la Protection des Données (RGPD), nous n’en tirons aucune utilité.

  En effet, Framasoft s’engage à ne faire aucune utilisation de vos données, hors besoins statistiques de base ou besoins techniques légitimes. C’est à dire que nous ne revendons pas vos données, et **nous nous fichons éperdument de savoir qui vous êtes ou de valoriser vos profils de données ou vos contenus**. C’est même l’un des sujets au cœur de notre association : démontrer qu’il est tout à fait possible d’opérer des services en ligne, sans chercher à profiter des informations communiquées par nos utilisateur⋅ices.

  La contrepartie de cette confiance, c’est que nous vous demandons, lorsque VOUS créez des contenus à l’aide de nos outils et services, de prêter attention à ne pas stocker ou divulguer d’informations personnelles sans obtenir de consentement préalable et explicite. Formulé différemment : **si vous devenez utilisateur ou utilisatrice de la plateforme @:txt.space, vous vous engagez à respecter le cadre fixé par le RGPD**.

  ## Politique de confidentialité

  La plateforme numérique @:txt.space et les services qui lui sont associés peuvent constituer un traitement de données à caractère personnel mis en œuvre par l’association Framasoft (10 bis rue Jangot, 69007 Lyon) pour l’exécution d’une mission d’intérêt légitime au sens du f) de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD).

  Framasoft s’engage à traiter vos données à caractère personnel dans le respect de la loi n°78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés et du RGPD.

  ### Quelles sont les données à caractère personnel collectées ? Pourquoi ? Combien de temps sont elles conservées ?

  #### Cookies, données statistiques, traçabilité

  Lors de leur navigation sur ces les sites de la plateforme @:txt.space, les internautes laissent des traces informatiques. Cet ensemble d’informations est recueilli à l’aide d’un témoin de connexion appelé cookie.

  Les données générées par les cookies sont transmises à Framasoft. Les données sont hébergées sur des serveurs de Framasoft, dont le prestataire technique est la société Hetzner (Allemagne).

  Les cookies sont déposés sur l’équipement de l’internaute. Ce sont :
  - les cookies fonctionnels permettent de personnaliser l’utilisation du site (par exemple mémoriser un mode de présentation) ;
  - les cookies destinés à la mesure d’audience. Ils ne collectent pas de données personnelles. Les outils de mesures d’audience sont déployés afin d’obtenir des informations sur la navigation des visiteurs. Ils permettent notamment de comprendre comment les utilisateurs arrivent sur un site et de reconstituer leur parcours.

  Vous avez la possibilité de refuser l’enregistrement de ces données en modifiant la configuration du navigateur de votre équipement ou de les supprimer sans que cela ait une quelconque influence sur votre accès aux pages du site et aux services.

  Vous pouvez vous opposer à l’enregistrement de cookies en configurant les paramètres de votre navigateur.

  **Finalité :** Afin de mesurer l’audience des services et contenus éditoriaux, nous utilisons une solution gestionnaire des statistiques : [Matomo](https://matomo.org)

  Note : cette solution est un logiciel libre. Framasoft opère et maintient sa propre instance Matomo, sur son infrastructure technique. Les données statistiques sont donc hébergées par Framasoft.

  La configuration de notre logiciel de mesure de l’audience a été effectuée en suivant les recommandations la CNIL, et nous implémentons la fonctionnalité “Do Not Track” réglable dans les navigateurs.

  **Détail des données recueillies** : équipement, navigateur utilisé, origine géographique des requêtes limitées aux pays, date et heure de la connexion, navigation sur le site, fréquence des visites, adresse IP anonymisée par suppression des deux derniers octets de l’adresse IP.

  **Durée de conservation** : Le cookie d’analyse a une durée de vie de 13 mois dans vos navigateurs. Les données collectées de façon anonyme sont conservées 12 mois.

  #### Journalisation

  Les journaux de connexion (« logs ») sont des fichiers textes enregistrant de manière chronologique les évènements exécutés par un serveur ou une application informatique.  Les événements peuvent être des activités des utilisateurs, des anomalies et des événements liés à la sécurité d’un environnement informatique (logiciel, système d’exploitation, application, site internet etc.).

  **Finalité :** Leur conservation est obligatoire dans certains cas et fortement recommandée pour des raisons de sécurité. L’objectif est de garantir le bon usage du système informatique.

  **Détail des données recueillies** : adresse IP, navigateur, user-agent, URL d’action (GET/POST), date de consultation

  **Durée de conservation** : les journaux de connexion sont conservés pour une période de 12 mois glissants pour répondre aux exigences légales. En cas d’extinction du service, les données sont conservées 12 mois maximum.

  #### Données du compte utilisateur

  #####   Pour le service « Espace @:txt.space » (basé sur le logiciel libre Nextcloud)

  Ces données sont récoltées soit lors de la demande de création d’un espace @:txt.space (compte administrateur) ou lorsque ce dernier invite des utilisateurs à rejoindre son espace.

  **Finalité :** Elles permettent d’une part à l’utilisateur ou l’administrateur de pouvoir se connecter au service, et d’autre part aux utilisateurs de pouvoir interagir entre eux. À des fins d’exploitation technique courante de la plateforme, ou afin d’améliorer le service @:txt.space, Framasoft pourra aussi être amené à utiliser ces données.

  **Détail des données recueillies :**

  NB : la plupart de ces informations peuvent être obligatoires ou facultatives, ainsi qu’être partagées de façon privée (seulement visible des utilisateurs mis en correspondance via leur numéro de téléphone par Talk sur mobile), locale (visible uniquement aux personnes dans l’instance et aux invités), fédérée (synchronisée uniquement avec les serveurs de confiance), publiée (synchronisation avec les serveurs de confiance et le carnet d’adresses mondial et public)

  **Identité**
  - email (**obligatoire**, local)
  - nom et prénom (local par défaut)
  - photo de profil (avatar, local par défaut)

  **Personnelle** (ex. habitudes de vie, situation familiale)
  - langue (**obligatoire**, privé)
  - numéro de téléphone (local par défaut)
  - adresse postale (local par défaut)
  - site web (local par défaut)
  - compte Twitter (local par défaut)

  **Durée de conservation :** ces données sont conservées dans le cadre de la procédure de sauvegarde jusqu’à 2 mois après la suppression du compte utilisateur (suite à une demande de suppression de sa part, ou suite à une suppression de son compte par l’administrateur du compte @:txt.space, ou par Framasoft elle-même).

  ##### Pour le service « Forum @:txt.space » (basé sur le logiciel libre Discourse)

  Ces données sont récoltées lors de la demande de création d’un compte sur le forum de discussion de la plateforme @:(txt.space).

  **Finalité :** Elles permettent d’une part à l’utilisateur de pouvoir se connecter au service, et d’autre part aux utilisateurs de pouvoir interagir entre eux. À des fins d’exploitation technique courante de la plateforme, ou afin d’améliorer le service @:txt.space, Framasoft pourra aussi être amené à utiliser ces données.

  **Détail des données recueillies :** email, Nom (ou pseudo)

  Facultativement, et à la volonté de l’utilisateur, ce dernier pourra renseigner d’autres données à caractère personnel le concernant : nom, prénom, téléphone, adresse, photographie.

  **Durée de conservation :** ces données sont conservées dans le cadre de la procédure de sauvegarde jusqu’à 2 mois après la suppression du compte utilisateur (suite à une demande de suppression de sa part,  ou par Framasoft elle-même).

  ##### Pour le service « Lettre d’information @:txt.space » (basé sur le logiciel libre ListMonk)

  Ces données sont récoltées lors de la demande d’inscription à la lettre d’information de la plateforme @:(txt.space).

  **Finalité :** Elles permettent à l’utilisateur de pouvoir recevoir une lettre d’information concernant les actualités et annonces relatives à la plateforme @:(txt.space). À des fins d’exploitation technique courante de la plateforme, ou afin d’améliorer le service @:txt.space, Framasoft pourra aussi être amené à utiliser ces données.

  **Détail des données recueillies :** email

  **Durée de conservation :** ces données sont conservées dans le cadre de la procédure de sauvegarde jusqu’à 2 mois après la suppression du compte utilisateur (suite à une demande de suppression de sa part,  ou par Framasoft elle-même).

  #### Données produites par les utilisateurs identifiés ou sous leurs responsabilité

  Les services @:txt.space permettant l’hébergement et la production libres de documents informatiques, une liste ne saurait être exhaustive. Framasoft, en tant qu’hébergeur technique de ces données est qualifié de sous-traitant de ces données et ne saurait être tenu pour être responsable de leurs traitement par les utilisateurs. Cependant, par volonté et devoir de respecter au mieux les engagements d’information et de transparence, chaque utilisateur s’inscrivant sur le service est invité à lire et à accepter les « [Conditions spécifiques d’hébergement des données à caractère personnel](https://www.frama.space/abc/fr/csu/) » relatives au traitement des données personnelles qui pourraient être réalisées par les utilisateurs du services @:(txt.space).

  **Finalité :** l’objectif même de @:txt.space est de proposer des espaces d’hébergement de données à des organisations. Ces données peuvent ne comporter aucun caractère personnel (ex: supports de communication promotionnels). Cependant, certains utilisateurs peuvent avoir l’utilité d’héberger des données dont le caractère peut être considéré comme personnel.

  **Détail des données recueillies : inconnu** (comme indiqué ci-dessus, chaque utilisateur peut créer de nouveaux fichiers/données, dont il a la responsabilité)

  **Durée de conservation :** ces données sont conservées dans le cadre de la procédure de sauvegarde jusqu’à 2 mois après la suppression du compte utilisateur (suite à une demande de suppression de sa part, ou suite à une suppression de son compte par l’administrateur du compte @:txt.space, ou par Framasoft elle-même).

  ### Qui peut consulter et partager mes données à caractère personnel lorsque j’utilise @:txt.space ?

  Les catégories de destinataires des données à caractère personnel sont :

  * les administrateurs système habilités par Framasoft ;
  * les administrateurs fonctionnels habilités par Framasoft ;
  * les administrateurs propriétaires de l’espace @:txt.space qui héberge vos données (ce sont eux qui font office de « Responsable de Traitement » dans le cadre du RGPD)
  * chacun des utilisateurs identifiés pour ses propres données ;
  * les autres utilisateurs identifiés (lorsque les données sont restreintes à ce public) ;
  * le grand public pour toutes les données rendues volontairement publiques par les utilisateurs ;
  * l’hébergeur technique, sous-traitant de Framasoft (société Hetzner GmbH).

  Si des utilisateurs de @:txt.space collectent des données à caractère personnel de leur propre initiative, nous rappelons que ces derniers se sont engagés (par consentement actif) à respecter nos « [Conditions spécifiques d’hébergement des données à caractère personnel](https://www.frama.space/abc/fr/csu/) » qui, pour résumer, les engagent à respecter le RGPD.

  Si vous deviez constater une infraction ou une violation des droits mis en œuvre par les « Conditions spécifiques d’hébergement des données à caractère personnel », nous vous enjoignons à contacter l’administrateur ou le responsable de traitement tel qu’indiqué aux articles 7 et 8 de nos « Conditions spécifiques d’hébergement des données à caractère personnel ».

  ### Comment partageons-nous les données à caractère personnel ?

  Simple : nous ne les partageons pas !

  **L’association Framasoft prend l’engagement que les données à caractère personnel qu’elle est amenée à collecter par elle-même ne seront jamais partagées à des fins commerciales ou marketing.**

  Le seul tiers (aussi appelé « sous-traitant ultérieur » selon les termes de la CNIL et du RGPD) qui est amené à manipuler vos données est notre partenaire technique, en charge de l’hébergement physique de nos serveurs informatiques. Il s’agit de l’entreprise européenne Hetzner :

  Hetzner Online GmbH<br />
  Industriestr. 25<br />
  91710 Gunzenhausen<br />
  Germany

  Vous pouvez lire ses engagements concernant la gestion des données personnelles ici [https://www.hetzner.com/legal/privacy-policy](https://www.hetzner.com/legal/privacy-policy) ainsi que leur FAQ ici : [https://docs.hetzner.com/general/general-terms-and-conditions/data-privacy-faq/](https://docs.hetzner.com/general/general-terms-and-conditions/data-privacy-faq/)

  Veuillez noter qu’Hetzner n’est pas certifié « Hébergeur de données de santé » (HDS).

  L’autre cas spécifique qui peut se présenter est celle d’une requête de police judiciaire (« réquisition à personne qualifiée ») qui serait faite auprès de Framasoft. Dans ce cas précis, nous pouvons être amené à partager l’information avec l’autorité compétente, avec les contraintes et limites indiquées dans [notre FAQ](https://www.frama.space/abc/fr/faq)

  Dans ce cas, Framasoft s’engage à :
  * Solliciter une réquisition écrite ;
  * Vérifier la nature des documents ou informations sollicitées (données administratives ou médicales) ;
  * Répondre à la réquisition positivement ou négativement pour ne pas encourir d’amende (s’il s’agit d’un dossier médical, il convient pour le médecin de refuser la réquisition et solliciter une saisie du dossier) ;
  * Faire une copie des pièces originales transmises.

  Si vous deviez constater une infraction ou une violation des droits explicités dans nos « [Conditions spécifiques d’hébergement des données à caractère personnel](https://www.frama.space/abc/fr/csu/) », nous vous enjoignons à contacter l’administrateur ou le responsable de traitement tel qu’indiqué aux articles 7 et 8 desdites Conditions spécifiques.

  ### Droits et choix en matière de confidentialité

  Vous pouvez accéder aux données vous concernant et exercer vos droits d’accès, de rectification, de limitation, d’opposition que vous tenez des articles 15, 16, 18 et 21 du RGPD, par courriel auprès de Framasoft. Pour plus d’informations, vous pouvez consulter l’adresse suivante : [https://framasoft.org/fr/legals/](https://framasoft.org/fr/legals/)

  De la même manière, vous pouvez exercer les droits prévus à l’article 85 de la loi n° 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés.

  ### Personnes mineures

  Rappel de ce que dit la CNIL : <[https://www.cnil.fr/fr/recommandation-1-encadrer-la-capacite-dagir-des-mineurs-en-ligne](https://www.cnil.fr/fr/recommandation-1-encadrer-la-capacite-dagir-des-mineurs-en-ligne)>

  *« Le RGPD et la loi Informatique et Libertés permettent au mineur de plus de 15 ans de donner, seul, son accord à certains traitements de données personnelles qui reposent sur un « consentement » non contractuel. Le mineur de plus de 15 ans peut ainsi légalement décider seul d’accepter les cookies pour consulter un site internet, d’opter pour un profil public ou privé sur un réseau social ou d’activer une fonctionnalité optionnelle de géolocalisation sur une application. En revanche, les textes ne reconnaissent pas une « majorité numérique globale » à 15 ans. Le RGPD ne consacre donc pas la capacité du mineur à s’inscrire seul sur un réseau social. En principe, le droit commun de la minorité s’applique, et les actions du mineur relèvent de l’autorité parentale, selon des modalités qui dépendent du contexte et ont été précisées par la jurisprudence. »*

  Framasoft estime raisonnablement que les conditions recommandées par la CNIL sont réunies.

    « Dès lors, elle estime que, sous réserve de l’appréciation souveraine des tribunaux, il serait cohérent que les mineurs puissent être considérés, en fonction de leur niveau de maturité et en tout état de cause à partir de 15 ans, comme capables de conclure des contrats ayant pour objet le traitement de leurs données dans le cadre de services en ligne, tels que l’inscription à un réseau social ou à un site de jeux en ligne), si et seulement si :
     - ces services sont adaptés aux publics mineurs qu’ils accueillent ;
     - ces traitements respectent strictement les règles de protection des données personnelles telles que fixées par le RGPD et la loi Informatique et Libertés (minimisation des données collectées, pour une finalité bien déterminée, une durée limitée et de manière sécurisée…) ;
     - le mineur est informé de façon claire et adaptée des conditions d’utilisation de ses données et de ses droits informatique et libertés, afin qu’il puisse comprendre le sens et la portée de son engagement ;
     - les parents disposent d’une voie de recours pour demander la suppression du compte de leur enfant s’ils l’estiment nécessaire afin de protéger son intérêt supérieur. »

  **Nous considérons donc que les mineurs de 15 ans et plus peuvent utiliser le service @:(txt.space).**

  ### Comment nous contacter

  Pour toute question sur le traitement de vos données dans ce dispositif, vous pouvez contacter le délégué à la protection des données de l’association Framasoft :

  * à l’adresse électronique suivante : dpo@framasoft.org
  * via le formulaire de saisine en ligne : [https://contact.framasoft.org/#donnees\_personnelles](https://contact.framasoft.org/#donnees\_personnelles)
  * ou par courrier en vous adressant au : Framasoft, 10 bis rue Jangot, 69007 LYON
    Si vous estimez, après nous avoir contactés, que vos droits ne sont pas respectés ou que ce dispositif n’est pas conforme aux règles de protection des données, vous pouvez adresser une réclamation auprès de la Commission nationale de l’informatique et des libertés (CNIL).

  # Je souhaite en savoir plus !

  #### Registre des activités de traitement

  Framasoft vous partage publiquement son registre des activités de traitement :

  [https://asso.framasoft.org/nextcloud/s/495Tz3m46j6f5sm](https://asso.framasoft.org/nextcloud/s/495Tz3m46j6f5sm) (ATTENTION : travail en cours)

  #### « Bonnes pratiques » et « Règles d’or »

  Chaque traitement de données à caractère personnel doit répondre à des conditions : il s’agit des 8 règles d’or de la protection des données personnelles. Ci-dessous, ces 8 règles d’or vous sont rappelées et nous indiquons comment Framasoft y répond pour son service @:(txt.space).

  Par ailleurs, nous appliquons les bonnes pratiques recommandées par la CNIL pour répondre aux exigences du RGPD en la matière :

  ✔ Framasoft ne collecte que les données vraiment nécessaires ;

  ✔ Nous sommes transparents avec l’ensemble de nos parties prenantes ;

  ✔ Nous respectons le droit des personnes, comme par exemple les droits accès, de suppression, ou de rectification ;

  ✔ Nous sécurisons nos données, donc nous sécurisons les vôtres. Étant entendu qu’il n’existe pas de sécurité parfaite en informatique.

  ##### 1. Finalité du traitement

  *La finalité du traitement doit être légitime et explicitement définie. La finalité du traitement doit être communiquée à l’avance et il est interdit de détourner la finalité d’une donnée.*

  Nous avons besoin de ces données personnelles (votre email, notamment) pour faire fonctionner techniquement le service.

  Ces données ne sont ni transmises, ni exploitées commercialement, ni revendues par Framasoft.

  ##### 2. Licéité du traitement

  *Le traitement doit être licite. Six conditions de licéité ont été définies afin d’expliciter cette notion [https://www.cnil.fr/fr/les-bases-legales/liceite-essentiel-sur-les-bases-legales](https://www.cnil.fr/fr/les-bases-legales/liceite-essentiel-sur-les-bases-legales).*

  La base légale du traitement répond à plusieurs points de l’article 6 du règlement général (UE) 2016/679 du Parlement européen et du Conseil du 27 avril 2016 sur la protection des données (RGPD) :

  * Article 6,a : les utilisateurs ont consenti activement au traitement de leurs données (case à cocher lors du formulaire d’inscription);
  * Article 6,b :  le traitement est nécessaire à l’exécution ou à la préparation d’un contrat avec la personne concernée (sans email de l’utilisateur, il est impossible de fournir le service) ;
  * Article 6,f :  le traitement est nécessaire à la poursuite d’intérêts légitimes de l’organisme, dans le strict respect des droits et intérêts des personnes dont les données sont traitées (l’email permet à Framasoft d’interagir avec les utilisateurs et d’améliorer le service rendu).

  ##### 3. Minimisation des données
  *Seules les données nécessaires à la finalité du traitement peuvent être collectées. Par exemple, il est interdit pour un service de livraison de demander des informations médicales. En effet, dans le cadre d’une livraison, les informations médicales n’aideront pas à atteindre la finalité. En revanche, le domicile du client est pertinent.*

  Les données à caractère personnel collectées (email, adresse IP, etc) constituent un socle minimal permettant d’exploiter le service @:(txt.space).

  Rappelons que Framasoft n’a aucun intérêt économique ou commercial à collecter et exploiter des données à caractère personnel.

  ##### 4. Protection particulière de certaines données

  *Les données sensibles doivent être collectées et traitées en respectant des conditions strictes. Par exemple, les données médicales nécessiteront des conditions bien spécifiques de traitement et de stockage car une fuite de ce type de données serait bien plus grave que la fuite d’un simple fichier client.*

  Aucune donnée sensible n’est collectée par Framasoft au niveau de l’exploitation du service @:(txt.space).

  ##### 5. Conservation limitée des données

  *Une fois la finalité atteinte, les données doivent être archivées, supprimées ou anonymisées. Ces opérations doivent répondre à des obligations légales, notamment sur l’archivage : les hébergeurs ont une obligation légale de conservation des données. Ainsi, une donnée doit être archivée durant un certain temps avant de pouvoir être supprimée. Les données archivées doivent toutefois rester séparées des données encore sujettes au traitement.*

  Nous conservons les données sur des périodes qui nous semble raisonnables pour pouvoir rendre le service @:txt.space fonctionnel (techniquement et légalement).

  Ces durées de conservations sont indiquées aux utilisateurs, par transparence, plus haut dans ce document.

  Des procédures automatisées permettent de nous assurer que nous ne conservons pas ces données plus longtemps qu’indiquées.

  ##### 6. Obligation de sécurité

  *Le responsable de traitement doit mettre en place une sécurité adaptée à la sensibilité des données. Même pour des données peu sensibles, il faut assurer une sécurité suffisante pour éviter une fuite.*

  Le RGPD dit :

  *« Compte tenu de l’état des connaissances, des coûts de mise en œuvre et de la nature, de la portée, du contexte et des finalités du traitement ainsi que des risques, dont le degré de probabilité et de gravité varie, pour les droits et libertés des personnes physiques, le responsable du traitement et le sous-traitant mettent en œuvre les mesures techniques et organisationnelles appropriées afin de garantir un niveau de sécurité adapté au risque […] »* -- Article 32 du RGPD

  La loi dit que :

  *« Le responsable du traitement est tenu de prendre toutes précautions utiles, au regard de la nature des données et des risques présentés par le traitement, pour préserver la sécurité des données et, notamment, empêcher qu’elles soient déformées, endommagées, ou que des tiers non autorisés y aient accès. »* -- Article 121 de la loi Informatique et Libertés

  Au regard de « bonnes pratiques », Framasoft a mis en place des mesures qui lui semblent proportionnées aux risques identifiés, en agissant sur :

  * **les « éléments à protéger »** : la collecte de données est minimisée, le transport des communications réseau est chiffrée (SSL), les informations personnelles peuvent être pseudonymisées, Framasoft permet aux utilisateurs de faire valoir l’exercice de leurs droits.
  * **les « impacts potentiels »** : les données sont sauvegardées, les activités sont tracées, les utilisateurs sont prévenus en cas de violation de données ou d’intrusion détectées.
  * **les « sources de risques »** : les accès aux serveurs sont contrôlés et réservés aux personnes habilités par Framasoft, les accès aux espaces de production utilisateurs sont contrôlés par les administrateurs d’espaces, Framasoft exerce une surveillance active contre les codes malveillants, Framasoft met régulièrement à jour les codes informatiques des logiciels utilisés dans leurs dernières versions.
  * **les « supports »** : les accès physiques au machines sont limités aux personnes habilités, afin de réduire les risques logiciels Framasoft n’utilise que des logiciels libres et opensource.

  ##### 7. Transparence

  *Les personnes doivent être informées du traitement de leurs données, de la condition justifiant de la licéité du traitement, la durée de conservation ainsi que de la manière d’exercer leurs droits (information ou suppression par exemple).*

  La page que vous êtes en train de lire nous semble un bon exemple de nos pratiques en termes de transparence.

  ##### 8. Droits des personnes
  *Chaque personne dispose de nombreux droits afin de conserver le contrôle de ses données personnelles :*
  * *Droit d’accès*
  * *Droit de rectification*
  * *Droit d’opposition*
  * *Droit à l’effacement*

  Framasoft indique clairement, notamment sur cette page, comment les utilisateurs peuvent exercer leurs droits.

  Par ailleurs, l’exercice de ces droits est détaillé dans les « [Conditions spécifiques d’hébergement des données à caractère personnel](https://www.frama.space/abc/fr/csu/) ».

  Enfin, il nous semble que les procédures pour les exercer lesdits droits sont simples et accessibles au plus grand nombre.

  ------

  <p class="text-right">
    <sub>Date de publication : 05/10/2022 - Date de dernière mise à jour : 06/10/2022 </sub><br />
    <sub>[Historique](https://framagit.org/framasoft/@:txt.space/home/-/commits/master/src/translations/fr/rgpd.yml)</sub>
  </p>
